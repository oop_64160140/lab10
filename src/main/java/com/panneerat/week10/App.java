package com.panneerat.week10;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
       Rectangle rec1 = new Rectangle(5, 3);
       System.out.println(rec1.toString());
       System.out.println("ares: " + rec1.calArea());
       System.out.println("perimeter: " + rec1.calPerimeter());

       Rectangle rec2 = new Rectangle(2, 2);
       System.out.println(rec2.toString());
       System.out.println("ares: " + rec2.calArea());
       System.out.println("perimeter: " + rec2.calPerimeter());

       Circle circle1 = new Circle(2);
       System.out.println(circle1);
       System.out.printf("%s ares: %.3f \n",circle1.getName(), circle1.calArea());
       System.out.printf("%s perimeter: %.3f \n",circle1.getName(), circle1.calPerimeter());

       Circle circle2 = new Circle(3);
       System.out.println(circle2);
       System.out.printf("%s ares: %.3f \n", circle1.getName(),circle2.calArea());
       System.out.printf("%s perimeter: %.3f \n",circle1.getName(), circle2.calPerimeter());

       Triangle triangle1 = new Triangle(5, 5, 6);
       System.out.println(triangle1);
       System.out.println("ares: " + triangle1.calArea());
       System.out.println("perimeter: " + triangle1.calPerimeter());

       Triangle triangle2 = new Triangle(3, 4, 5);
       System.out.println(triangle2);
       System.out.println("ares: " + triangle2.calArea());
       System.out.println("perimeter: " + triangle2.calPerimeter());
    }
}
