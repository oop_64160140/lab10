package com.panneerat.week10;

public class Triangle extends Shape {
    private double a;
    private double b;
    private double c;
    public Triangle(double a, double b, double c){
        super("Triangle");
        this.a = a;
        this.b = b;
        this.c = c;
    }
    public Double getA(){
        return a;
    }
    public Double getB(){
        return b;
    }
    public Double getC(){
        return c;
    }

    @Override
    public double calArea() {
        double s = (a+b+c)/2;
        return Math.sqrt(s*(s-a)*(s-b)*(s-c));
    }
    @Override
    public double calPerimeter() {
        return a+b+c;
    }

    @Override 
    public String toString(){
        return this.getName() + " a: " + this.a + " b: " + this.b + " c: " + this.c;
    }
}
